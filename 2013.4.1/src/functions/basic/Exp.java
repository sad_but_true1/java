/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions.basic;

import functions.Function;

/**
 *
 * @author true
 */
public class Exp implements Function {

    public double getLeftDomainBorder() {
        return Double.NEGATIVE_INFINITY;
    }

    public double getRightDomainBorder() {
        return Double.POSITIVE_INFINITY;
    }

    public double getFunctionValue(double x) {
        return Math.exp(x);
    }
}
