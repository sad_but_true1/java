/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions.meta;

import functions.Function;

/**
 *
 * @author true
 */
public class Sum implements Function {

    private Function func1, func2;

    public Sum(Function func1, Function func2) {
        this.func1 = func1;
        this.func2 = func2;
    }

    public double getLeftDomainBorder() {
        double func1Border = func1.getLeftDomainBorder();
        double func2Border = func2.getLeftDomainBorder();
        if (func1Border > func2Border) {
            return func1Border;
        } else {
            return func2Border;
        }
    }

    public double getRightDomainBorder() {
        double func1Border = func1.getRightDomainBorder();
        double func2Border = func2.getRightDomainBorder();
        if (func1Border < func2Border) {
            return func1Border;
        } else {
            return func2Border;
        }
    }

    public double getFunctionValue(double x) {
        return func1.getFunctionValue(x) + func2.getFunctionValue(x);
    }
}
