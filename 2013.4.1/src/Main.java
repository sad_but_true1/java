/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import functions.*;
import functions.basic.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 *
 * @author true
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Function testFunction = new Sin();
        System.out.println("Sin:");
        for (double i = 0; i < 2 * Math.PI; i += 0.1) {
            System.out.println(testFunction.getFunctionValue(i));
        }
        testFunction = new Cos();
        System.out.println("Cos:");
        for (double i = 0; i < 2 * Math.PI; i += 0.1) {
            System.out.println(testFunction.getFunctionValue(i));
        }
        TabulatedFunction functionTestSin = TabulatedFunctions.tabulate(new Sin(), 0, 2 * Math.PI, 10);
        System.out.println("Tabulated sin:");
        functionTestSin.print();
        TabulatedFunction functionTestCos = TabulatedFunctions.tabulate(new Cos(), 0, 2 * Math.PI, 10);
        System.out.println("Tabulated cos:");
        functionTestCos.print();
        testFunction = Functions.sum(Functions.power(functionTestSin, 2), Functions.power(functionTestCos, 2));
        System.out.println("Sum of squares of tabuleted sin and cos:");
        for (double i = 0; i < 2 * Math.PI; i += 0.1) {
            System.out.println(testFunction.getFunctionValue(i));
        }
        functionTestSin = TabulatedFunctions.tabulate(new Sin(), 0, 2 * Math.PI, 30);
        functionTestCos = TabulatedFunctions.tabulate(new Cos(), 0, 2 * Math.PI, 30);
        testFunction = Functions.sum(Functions.power(functionTestSin, 2), Functions.power(functionTestCos, 2));
        System.out.println("Sum of squares of tabuleted sin and cos (30):");
        for (double i = 0; i < 2 * Math.PI; i += 0.1) {
            System.out.println(testFunction.getFunctionValue(i));
        }
        testFunction = TabulatedFunctions.tabulate(new Exp(), 0, 10, 11);
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("exp")));
            TabulatedFunctions.writeTabulatedFunction((TabulatedFunction) testFunction, out);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Before file:");
        ((TabulatedFunction) testFunction).print();
        try {
            BufferedReader in = new BufferedReader(new FileReader("exp"));
            testFunction = TabulatedFunctions.readTabulatedFunction(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("After file:");
        ((TabulatedFunction) testFunction).print();
        testFunction = TabulatedFunctions.tabulate(new Log(Math.E), 0, 10, 11);
        try {
            OutputStream out1 = new DataOutputStream(new FileOutputStream("ln.bin"));
            TabulatedFunctions.outputTabulatedFunction((TabulatedFunction) testFunction, out1);
            out1.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Before file:");
        ((TabulatedFunction) testFunction).print();
        try {
            InputStream in1 = new DataInputStream(new FileInputStream("ln.bin"));
            testFunction = TabulatedFunctions.inputTabulatedFunction(in1);
            in1.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("After file:");
        ((TabulatedFunction) testFunction).print();
        testFunction = TabulatedFunctions.tabulate(Functions.composition(new Log(Math.E), new Exp()), 0, 10, 11);
        try {
            ObjectOutputStream out2 = new ObjectOutputStream(new FileOutputStream("ln(exp).out"));
            out2.writeObject(testFunction);
            out2.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Before file:");
        ((TabulatedFunction) testFunction).print();
        try {
            ObjectInputStream in2 = new ObjectInputStream(new FileInputStream("ln(exp).out"));
            testFunction = (Function) in2.readObject();
            in2.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("After file:");
        ((TabulatedFunction) testFunction).print();
    }
}
