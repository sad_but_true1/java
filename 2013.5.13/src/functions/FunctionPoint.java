/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

import java.io.Serializable;

/**
 *
 * @author true
 */
public class FunctionPoint implements Serializable, Cloneable {

    public double x, y;

    public FunctionPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public FunctionPoint(FunctionPoint point) {
        x = point.x;
        y = point.y;
    }

    public FunctionPoint() {
        x = 0;
        y = 0;
    }

    public String toString() {
        StringBuilder tmp = new StringBuilder("(");
        tmp.append(x);
        tmp.append("; ");
        tmp.append(y);
        tmp.append(")");
        return tmp.toString();
    }

    public boolean equals(Object o) {
        if (!(o instanceof FunctionPoint)) {
            return false;
        }
        if (o == this) {
            return true;
        }
        return x == ((FunctionPoint) o).x && y == ((FunctionPoint) o).y;
    }

    public int hashCode() {
        //return Double.valueOf(x).hashCode() ^ Double.valueOf(y).hashCode();//засирает кучу
        long tmp = Double.doubleToLongBits(x);
        int hash = ((int) (tmp & 0xFFFFFFFFL)) ^ ((int) (tmp >> 32));
        tmp = Double.doubleToLongBits(y);
        hash ^= ((int) (tmp & 0xFFFFFFFFL)) ^ ((int) (tmp >> 32));
        return hash;
    }

    public Object clone() {
        Object result = null;
        try {
            result = super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
        return result;
    }
}
