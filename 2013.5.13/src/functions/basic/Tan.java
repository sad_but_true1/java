/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions.basic;

/**
 *
 * @author true
 */
public class Tan extends TrigonometricFunction {

    public double getFunctionValue(double x) {
        return Math.tan(x);
    }
}
