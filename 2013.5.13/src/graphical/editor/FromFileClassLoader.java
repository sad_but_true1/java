/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphical.editor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author true
 */
public class FromFileClassLoader extends ClassLoader {

    public Class loadClassFromFile(String fileName) throws IOException{
        byte[] b = loadClassData(fileName);
        return defineClass(null, b, 0, b.length);

    }

    private byte[] loadClassData(String fileName) throws FileNotFoundException, IOException {
        byte[] fileContent;
        try (FileInputStream in = new FileInputStream(fileName)) {
            fileContent = new byte[in.available()];
            in.read(fileContent);
        }
        return fileContent;
    }
}
