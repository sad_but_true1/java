/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import functions.basic.Log;
import functions.Function;
import static java.lang.Math.random;

/**
 *
 * @author true
 */
public class SimpleGenerator implements Runnable {

    private final Task task;

    public SimpleGenerator(Task task) {
        this.task = task;
    }

    @Override
    public void run() {
        for (int index = 0; index < task.count; index++) {
            Function log = new Log(1 + random() * 9);
            double leftBorder = random() * 100;
            double rightBorder = random() * 100 + 100;
            double delta = random();
            synchronized (task) {
                task.func = log;
                task.leftBorder = leftBorder;
                task.rightBorder = rightBorder;
                task.delta = delta;
                System.out.println("Source " + task.leftBorder + " " + task.rightBorder + " " + task.delta);
            }
        }
    }
}
