/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import functions.Function;
import functions.basic.Log;
import static java.lang.Math.random;

/**
 *
 * @author true
 */
public class Generator extends Thread {

    private final Task task;
    private Semaphore semaphore;

    public Generator(Task task, Semaphore semaphore, String name) {
        super(name);
        this.task = task;
        this.semaphore = semaphore;
    }

    public void run() {
        for (int index = 0; index < task.count && !isInterrupted(); index++) {
            Function log = new Log(1 + random() * 9);
            double leftBorder = random() * 100;
            double rightBorder = random() * 100 + 100;
            double delta = random();
            try {
                semaphore.beginWrite();
            } catch (InterruptedException ex) {
                interrupt();
            }
            task.func = log;
            task.leftBorder = leftBorder;
            task.rightBorder = rightBorder;
            task.delta = delta;
            System.out.println("Source " + task.leftBorder + " " + task.rightBorder + " " + task.delta);
            semaphore.endWrite();
        }
    }
}
