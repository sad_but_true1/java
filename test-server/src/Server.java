import java.io.*;
import java.net.*;

public class Server {

    public static void main(String[] args) throws IOException {
        System.out.println("Welcome to Server side");

        ServerSocket servers = null;

        // create server socket on port 9006
        try {
            servers = new ServerSocket(9006);
        } catch (IOException e) {
            System.out.println("Couldn't listen to port 9006");
            System.exit(-1);
        }

        boolean quit = false;
        while (!quit){
            //creating socket for client
            Socket       fromclient = null;
            try {
                System.out.print("Waiting for a client...");
                fromclient= servers.accept();
                System.out.println("Client connected");
            } catch (IOException e) {
                System.out.println("Can't accept");
                System.exit(-1);
            }

            //creating i/o streams
            BufferedReader in = new BufferedReader(new InputStreamReader(fromclient.getInputStream()));
            PrintWriter    out = new PrintWriter(fromclient.getOutputStream(),true);
            String         input;

            System.out.println("Wait for messages");
            while ((input = in.readLine()) != null) {
                if (input.equalsIgnoreCase("exit")) break;
                if (input.equalsIgnoreCase("kill")){quit = true; break;}
                long x;
                try {
                    x = Long.parseLong(input) * 2;
                    out.println("S ::: "+input+'*'+2+'='+x);
                    System.out.println(input+'*'+2+'='+x);
                } catch (NumberFormatException ex) {
                    out.println("S ::: Not a number!");
                    System.out.println("Not a number !");
                }
            }
            out.close();
            in.close();
            fromclient.close();
        }
        servers.close();
    }
}