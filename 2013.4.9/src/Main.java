/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import functions.*;

/**
 *
 * @author true
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayTabulatedFunction testArr = new ArrayTabulatedFunction(0, 10, 11);
        System.out.println(testArr.toString());
        LinkedListTabulatedFunction testList = new LinkedListTabulatedFunction(-2, 10, 13);
        System.out.println(testList.toString());
        System.out.println(testArr.equals(testList));
        System.out.println(testList.equals(testList));
        System.out.println(testArr.equals(new FunctionPoint()));
        LinkedListTabulatedFunction testList1 = new LinkedListTabulatedFunction(0, 10, 11);
        System.out.println(testArr.equals(testList1));
        System.out.println(testList1.equals(testArr));
        System.out.println(testArr.hashCode());
        System.out.println(testList.hashCode());
        System.out.println(testList1.hashCode());
        testArr.setPointY(1, 0.003);
        System.out.println(testArr.hashCode());
        testArr.setPointY(1, 0.1);
        System.out.println(testArr.hashCode());
        TabulatedFunction copy = (LinkedListTabulatedFunction)testList.clone();
        testList.setPointY(4, 10);
        copy.deletePoint(4);//Проверка, не поломал ли LinkedListTabulatedFunction.copy() логику работы списка
        try {
            copy.setPointX(1, -0.2);
        } catch (FunctionPointIndexOutOfBoundsException | InappropriateFunctionPointException ex) {
            System.err.println(ex);
        }
        System.out.println("Copy");
        System.out.println(copy);//Конец проверки 
        System.out.println("Original");
        System.out.println(testList);
        System.out.println(testList.toString());
        System.out.println(copy.toString());
        copy = (ArrayTabulatedFunction)testArr.clone();
        testArr.setPointY(2, 10);
        System.out.println(testArr.toString());
        System.out.println(copy.toString());
        System.out.println(copy.equals(null));
        double parseDouble = Double.parseDouble("kdsaf");
    }
}
