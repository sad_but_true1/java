/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

/**
 *
 * @author true
 */
public interface Function {

    double getLeftDomainBorder();

    double getRightDomainBorder();

    double getFunctionValue(double x);
}
