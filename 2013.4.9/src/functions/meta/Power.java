/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions.meta;

import functions.Function;

/**
 *
 * @author true
 */
public class Power implements Function {

    private Function func;
    private double power;

    public Power(Function func, double power) {
        this.power = power;
        this.func = func;
    }

    public double getLeftDomainBorder() {
        return func.getLeftDomainBorder();
    }

    public double getRightDomainBorder() {
        return func.getLeftDomainBorder();
    }

    public double getFunctionValue(double x) {
        return Math.pow(func.getFunctionValue(x), power);
    }   
}
