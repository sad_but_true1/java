/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

/**
 *
 * @author true
 */
public class LinkedListTabulatedFunction implements TabulatedFunction {

    private class FunctionNode {

        public FunctionPoint point;
        public FunctionNode next, prev;

        public FunctionNode() {
            point = new FunctionPoint();
        }

        public FunctionNode(FunctionPoint point) {
            this.point = new FunctionPoint(point);
        }

        public FunctionNode(FunctionPoint point, FunctionNode next, FunctionNode prev) {
            this.point = new FunctionPoint(point);
            this.next = next;
            this.prev = prev;
        }
    }
    private FunctionNode head, usedLast;
    private int pointsCount, indexUsedLast;

    private LinkedListTabulatedFunction() {
        head = new FunctionNode();
        head.next = head.prev = head;
        head.point = null;
        usedLast = head;
        pointsCount = 0;
        indexUsedLast = -1;
    }

    private FunctionNode getNodeByIndex(int index) {
        //учтена цикличность
        if (indexUsedLast < index){
            if (index - indexUsedLast < pointsCount - index - 1){
                while (indexUsedLast != index) {
                    usedLast = usedLast.next;
                    indexUsedLast++;
                }
            } else{
                usedLast = head.prev;
                indexUsedLast = pointsCount - 1;
                while (indexUsedLast != index) {
                    usedLast = usedLast.prev;
                    indexUsedLast--;
                }
            }
        } else{
            if (indexUsedLast - index < index){
                while (indexUsedLast != index) {
                    usedLast = usedLast.prev;
                    indexUsedLast--;
                }
            } else{
                usedLast = head.next;
                indexUsedLast = 0;
                while (indexUsedLast != index) {
                    usedLast = usedLast.next;
                    indexUsedLast++;
                }
            }
        }
        //TODO: список то у нас аняняня, циклический, учти это.
        /*if (indexUsedLast < index) {
            while (indexUsedLast != index) {
                usedLast = usedLast.next;
                indexUsedLast++;
            }
        } else {
            while (indexUsedLast != index) {
                usedLast = usedLast.prev;
                indexUsedLast--;
            }
        }*/
        return usedLast;
    }

    private FunctionNode addNodeToTail() {
        FunctionNode tmp;
        tmp = new FunctionNode(new FunctionPoint(), head, head.prev);
        head.prev.next = tmp;
        head.prev = tmp;
        pointsCount++;
        return tmp;
    }

    private FunctionNode addNodeByIndex(int index) {
        if (index == pointsCount) {
            return addNodeToTail();
        }
        FunctionNode insertionPlace = getNodeByIndex(index);
        FunctionNode tmp = new FunctionNode(new FunctionPoint(), insertionPlace, insertionPlace.prev);
        insertionPlace.prev.next = tmp;
        insertionPlace.prev = tmp;
        usedLast = tmp;
        indexUsedLast = index;
        pointsCount++;
        return tmp;
    }

    private FunctionNode deleteNodeByIndex(int index) {
        FunctionNode deletionPlace = getNodeByIndex(index);
        deletionPlace.prev.next = deletionPlace.next;
        deletionPlace.next.prev = deletionPlace.prev;
        usedLast = deletionPlace.prev;
        indexUsedLast--;
        pointsCount--;
        return deletionPlace;
    }

    public LinkedListTabulatedFunction(double leftX, double rightX, int pointsCount) {
        this();
        if (pointsCount < 2) {
            throw new IllegalArgumentException("Too few points.");
        }
        if (leftX >= rightX) {
            throw new IllegalArgumentException("Wrong left x.");
        }
        double delta = (rightX - leftX) / (pointsCount - 1);
        for (int i = 0; i < pointsCount; i++) {
            addNodeToTail().point.x = leftX + i * delta;
        }
    }

    public LinkedListTabulatedFunction(double leftX, double rightX, double[] values) {
        this();
        if (values.length < 2) {
            throw new IllegalArgumentException("Too few points.");
        }
        if (leftX >= rightX) {
            throw new IllegalArgumentException("Wrong left x.");
        }
        double delta = (rightX - leftX) / (values.length - 1);
        for (int i = 0; i < values.length; i++) {
            FunctionNode tmp = addNodeToTail();
            tmp.point.x = leftX + i * delta;
            tmp.point.y = values[i];
        }
    }

    public double getLeftDomainBorder() {
        return head.next.point.x;
    }

    public double getRightDomainBorder() {
        return head.prev.point.x;
    }

    public double getFunctionValue(double x) {
        if (x <= this.getRightDomainBorder() && x >= this.getLeftDomainBorder()) {
            for (int i = 0; i < pointsCount - 1; i++) {
                FunctionNode node = getNodeByIndex(i);
                if (x >= node.point.x) {
                    FunctionNode nextNode = getNodeByIndex(i + 1);
                    return (x - node.point.x) * (nextNode.point.y - node.point.y) / (nextNode.point.x - node.point.x) + node.point.y;
                }
            }
        }
        return Double.NaN;
    }

    public int getPointsCount() {
        return pointsCount;
    }

    public FunctionPoint getPoint(int index) {
        if (index < 0 || index >= pointsCount) {
            throw new FunctionPointIndexOutOfBoundsException("Wrong point index:" + index);
        }
        return new FunctionPoint(getNodeByIndex(index).point);
    }

    public double getPointX(int index) throws FunctionPointIndexOutOfBoundsException {
        if (index < 0 || index >= pointsCount) {
            throw new FunctionPointIndexOutOfBoundsException("Wrong point index.");
        }
        return getNodeByIndex(index).point.x;
    }

    public void setPointX(int index, double x) throws InappropriateFunctionPointException {
        if (index < 0 || index >= pointsCount) {
            throw new FunctionPointIndexOutOfBoundsException("Wrong point index.");
        }
        if (index == 0) {
            if (x < getNodeByIndex(index + 1).point.x) {
                getNodeByIndex(index).point.x = x;
            } else {
                throw new InappropriateFunctionPointException("Wrong x domain.");
            }
        } else if (index == pointsCount - 1) {
            if (x > getNodeByIndex(index - 1).point.x) {
                getNodeByIndex(index).point.x = x;
            } else {
                throw new InappropriateFunctionPointException("Wrong x domain.");
            }
        } else if (getNodeByIndex(index - 1).point.x < x && x < getNodeByIndex(index + 1).point.x) {
            getNodeByIndex(index).point.x = x;
        } else {
            throw new InappropriateFunctionPointException("Wrong x domain.");
        }
    }

    public double getPointY(int index) {
        if (index < 0 || index >= pointsCount) {
            throw new FunctionPointIndexOutOfBoundsException("Wrong point index.");
        }
        return getNodeByIndex(index).point.y;
    }

    public void setPointY(int index, double y) {
        if (index < 0 || index >= pointsCount) {
            throw new FunctionPointIndexOutOfBoundsException("Wrong point index.");
        }
        getNodeByIndex(index).point.y = y;
    }

    public void setPoint(int index, FunctionPoint point) throws InappropriateFunctionPointException {
        if (index < 0 || index >= pointsCount) {
            throw new FunctionPointIndexOutOfBoundsException("Wrong point index.");
        }
        double x = point.x;
        FunctionPoint pointCopy = new FunctionPoint(point.x, point.y);
        if (index == 0) {
            if (x <= getNodeByIndex(index + 1).point.x) {
                getNodeByIndex(index).point = pointCopy;
            } else {
                throw new InappropriateFunctionPointException("Wrong x domain.");
            }
        } else if (index == pointsCount - 1) {
            if (x >= getNodeByIndex(index - 1).point.x) {
                getNodeByIndex(index).point = pointCopy;
            } else {
                throw new InappropriateFunctionPointException("Wrong x domain.");
            }
        } else if (getNodeByIndex(index - 1).point.x <= x && x <= getNodeByIndex(index + 1).point.x) {
            getNodeByIndex(index).point = pointCopy;
        } else {
            throw new InappropriateFunctionPointException("Wrong x domain.");
        }
    }

    public void deletePoint(int index) {
        if (pointsCount - 1 < 3) {
            throw new IllegalStateException("To few points.");
        }
        if (index < 0 || index >= pointsCount) {
            throw new FunctionPointIndexOutOfBoundsException("Wrong deletion index.");
        }
        deleteNodeByIndex(index);
    }

    public void addPoint(FunctionPoint point) throws InappropriateFunctionPointException {
        int i = 1;
        FunctionNode tmp;
        if (pointsCount > 0) {
            tmp = head.next;
            while (tmp.point.x < point.x && i < pointsCount) {
                i++;
                tmp = tmp.next;
            }
            if (i < pointsCount && tmp.point.x == point.x) {
                throw new InappropriateFunctionPointException("Same X");
            }
            tmp = addNodeByIndex(i - 1);
        } else {
            tmp = addNodeToTail();
        }
        tmp.point.x = point.x;
        tmp.point.y = point.y;
    }

    public void print() {
        for (int i = 0; i < pointsCount; i++) {
            System.out.println("(" + this.getPointX(i) + ',' + this.getPointY(i) + ")");
        }
    }
}
