/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

/**
 *
 * @author true
 */
public interface TabulatedFunction {

    public void print();

    public double getLeftDomainBorder();

    public double getRightDomainBorder();

    public double getFunctionValue(double x);

    public int getPointsCount();

    public FunctionPoint getPoint(int index) throws FunctionPointIndexOutOfBoundsException;

    public double getPointX(int index) throws FunctionPointIndexOutOfBoundsException;

    public void setPointX(int index, double x) throws FunctionPointIndexOutOfBoundsException, InappropriateFunctionPointException;

    public double getPointY(int index) throws FunctionPointIndexOutOfBoundsException;

    public void setPointY(int index, double y) throws FunctionPointIndexOutOfBoundsException;

    public void setPoint(int index, FunctionPoint point) throws FunctionPointIndexOutOfBoundsException, InappropriateFunctionPointException;

    public void deletePoint(int index) throws FunctionPointIndexOutOfBoundsException, IllegalStateException;

    public void addPoint(FunctionPoint point) throws InappropriateFunctionPointException;
}
