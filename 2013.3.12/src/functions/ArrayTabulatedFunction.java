//убрать throws
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

/**
 *
 * @author true
 */
public class ArrayTabulatedFunction implements TabulatedFunction {

    private FunctionPoint arrayPoints[];
    private int pointsCount = 0;

    public void print() {
        for (int i = 0; i < pointsCount; i++) {
            System.out.println("(" + this.getPointX(i) + ',' + this.getPointY(i) + ")");
        }
    }

    public ArrayTabulatedFunction(double leftX, double rightX, int pointsCount) {
        if (pointsCount < 2) {
            throw new IllegalArgumentException("Too few points.");
        }
        if (leftX >= rightX) {
            throw new IllegalArgumentException("Wrong left x.");
        }
        this.pointsCount = pointsCount;
        arrayPoints = new FunctionPoint[2 * pointsCount];
        double delta = (rightX - leftX) / (pointsCount - 1);
        for (int i = 0; i < arrayPoints.length; i++) {
            arrayPoints[i] = new FunctionPoint(leftX + i * delta, 0);
        }
    }

    public ArrayTabulatedFunction(double leftX, double rightX, double[] values) {
        if (values.length < 2) {
            throw new IllegalArgumentException("Too few points.");
        }
        if (leftX >= rightX) {
            throw new IllegalArgumentException("Wrong left x.");
        }
        this.pointsCount = values.length;
        arrayPoints = new FunctionPoint[2 * pointsCount];
        double delta = (rightX - leftX) / (pointsCount - 1);
        for (int i = 0; i < values.length; i++) {
            arrayPoints[i] = new FunctionPoint(leftX + i * delta, values[i]);
        }
    }

    public double getLeftDomainBorder() {
        return arrayPoints[0].x;
    }

    public double getRightDomainBorder() {
        return arrayPoints[pointsCount - 1].x;
    }

    public double getFunctionValue(double x) {
        if (x <= this.getRightDomainBorder() && x >= this.getLeftDomainBorder()) {
            for (int i = 0; i < arrayPoints.length - 1; i++) {
                if (x >= arrayPoints[i].x) {
                    return (x - arrayPoints[i].x) * (arrayPoints[i + 1].y - arrayPoints[i].y) / (arrayPoints[i + 1].x - arrayPoints[i].x) + arrayPoints[i].y;
                }
            }
        }
        return Double.NaN;
    }

    public int getPointsCount() {
        return pointsCount;
    }

    public FunctionPoint getPoint(int index) {
        if (index < 0 || index >= pointsCount) {
            throw new FunctionPointIndexOutOfBoundsException("Wrong point index:" + index);
        }
        return new FunctionPoint(arrayPoints[index]);
    }

    public double getPointX(int index) throws FunctionPointIndexOutOfBoundsException {
        if (index < 0 || index >= pointsCount) {
            throw new FunctionPointIndexOutOfBoundsException("Wrong point index:" + index);
        }
        return arrayPoints[index].x;
    }

    public void setPointX(int index, double x) throws InappropriateFunctionPointException {
        if (index < 0 || index >= pointsCount) {
            throw new FunctionPointIndexOutOfBoundsException("Wrong point index:" + index);
        }
        if (index == 0) {
            if (x < arrayPoints[index + 1].x) {
                arrayPoints[index].x = x;
            } else {
                throw new InappropriateFunctionPointException("Wrong x domain.");
            }
        } else if (index == pointsCount - 1) {
            if (x > arrayPoints[index - 1].x) {
                arrayPoints[index].x = x;
            } else {
                throw new InappropriateFunctionPointException("Wrong x domain.");
            }
        } else if (arrayPoints[index - 1].x < x && x < arrayPoints[index + 1].x) {
            arrayPoints[index].x = x;
        } else {
            throw new InappropriateFunctionPointException("Wrong x domain.");
        }
    }

    public double getPointY(int index) {
        if (index < 0 || index >= pointsCount) {
            throw new FunctionPointIndexOutOfBoundsException("Wrong point index:" + index);
        }
        return arrayPoints[index].y;
    }

    public void setPointY(int index, double y) {
        if (index < 0 || index >= pointsCount) {
            throw new FunctionPointIndexOutOfBoundsException("Wrong point index:" + index);
        }
        arrayPoints[index].y = y;
    }

    public void setPoint(int index, FunctionPoint point) throws InappropriateFunctionPointException {
        if (index < 0 || index >= pointsCount) {
            throw new FunctionPointIndexOutOfBoundsException("Wrong point index:" + index);
        }
        double x = point.x;
        FunctionPoint pointCopy = new FunctionPoint(point.x, point.y);
        if (index == 0) {
            if (x <= arrayPoints[index + 1].x) {
                arrayPoints[index] = pointCopy;
            } else {
                throw new InappropriateFunctionPointException("Wrong x domain.");
            }
        } else if (index == pointsCount - 1) {
            if (x >= arrayPoints[index - 1].x) {
                arrayPoints[index] = pointCopy;
            } else {
                throw new InappropriateFunctionPointException("Wrong x domain.");
            }
        } else if (arrayPoints[index - 1].x <= x && x <= arrayPoints[index + 1].x) {
            arrayPoints[index] = pointCopy;
        } else {
            throw new InappropriateFunctionPointException("Wrong x domain.");
        }
    }

    public void deletePoint(int index) {
        if (pointsCount - 1 < 3) {
            throw new IllegalStateException("To few points.");
        }
        if (index < 0 || index >= pointsCount) {
            throw new FunctionPointIndexOutOfBoundsException("Wrong point index:" + index);
        }
        if (index < pointsCount - 1) {
            System.arraycopy(arrayPoints, index + 1, arrayPoints, index, pointsCount - 1 - index);
        }
        pointsCount--;
    }

    public void addPoint(FunctionPoint point) throws InappropriateFunctionPointException {
        FunctionPoint pointCopy = new FunctionPoint(point.x, point.y);
        if (pointsCount >= arrayPoints.length) {
            FunctionPoint tmp[];
            tmp = new FunctionPoint[arrayPoints.length * 3 / 2 + 1];
            System.arraycopy(arrayPoints, 0, tmp, 0, arrayPoints.length);
            arrayPoints = tmp;
        }
        int i = 0;
        while (arrayPoints[i].x < pointCopy.x && i < pointsCount) {
            i++;
        }
        if (i < pointsCount && arrayPoints[i].x == pointCopy.x) {
            throw new InappropriateFunctionPointException("Same X");
        }
        System.arraycopy(arrayPoints, i, arrayPoints, i + 1, pointsCount - i);
        pointsCount++;
        setPoint(i, pointCopy);
    }
}
