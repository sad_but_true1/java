/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import functions.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author true
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            double[] x = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            TabulatedFunction example;
            try {
                example = new ArrayTabulatedFunction(0, 10, 1);
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            }
            try {
                example = new ArrayTabulatedFunction(10, 0, 10);
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            }
            example = new ArrayTabulatedFunction(0, 10, x);
            try {
                example.getPointX(12);
            } catch (FunctionPointIndexOutOfBoundsException ex) {
                ex.printStackTrace();
            }
            try {
                example.setPointX(0, 10);
            } catch (InappropriateFunctionPointException ex) {
                ex.printStackTrace();
            }
            try {
                example.setPointX(-1, 10);
            } catch (FunctionPointIndexOutOfBoundsException ex) {
                ex.printStackTrace();
            } catch (InappropriateFunctionPointException ex) {
                ex.printStackTrace();
            }
            try {
                example.addPoint(new FunctionPoint(3, 29));
            } catch (InappropriateFunctionPointException ex) {
                ex.printStackTrace();
            }
            example = new ArrayTabulatedFunction(0, 10, 3);
            try {
                example.deletePoint(0);
            } catch (IllegalStateException ex) {
                ex.printStackTrace();
            }

            try {
                example = new LinkedListTabulatedFunction(0, 10, 1);
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            }
            try {
                example = new LinkedListTabulatedFunction(10, 0, 10);
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            }
            example = new LinkedListTabulatedFunction(0, 10, x);
            try {
                example.getPoint(12);
            } catch (FunctionPointIndexOutOfBoundsException ex) {
                ex.printStackTrace();
            }
            try {
                example.setPointX(0, 10);
            } catch (InappropriateFunctionPointException ex) {
                ex.printStackTrace();
            }
            try {
                example.addPoint(new FunctionPoint(3, 29));
            } catch (InappropriateFunctionPointException ex) {
                ex.printStackTrace();
            }
            example = new LinkedListTabulatedFunction(0, 10, 3);
            try {
                example.deletePoint(0);
            } catch (IllegalStateException ex) {
                ex.printStackTrace();
            }


           /* TabulatedFunction exampleTabFunc = new LinkedListTabulatedFunction(0, 10, x);
            for (double y = exampleTabFunc.getLeftDomainBorder() - 1; y < exampleTabFunc.getRightDomainBorder() + 2; y += 0.5) {
                System.out.println("(" + y + ',' + exampleTabFunc.getFunctionValue(y) + ")");
            }
            exampleTabFunc.setPointX(1, 0.9);
            exampleTabFunc.print();
            System.out.println();
            exampleTabFunc.setPointY(1, 3);
            exampleTabFunc.print();
            System.out.println();
            FunctionPoint examplePoint = new FunctionPoint(0, 22);
            exampleTabFunc.setPoint(0, examplePoint);
            exampleTabFunc.print();
            exampleTabFunc.deletePoint(4);
            System.out.println();
            exampleTabFunc.print();
            System.out.println();
            examplePoint = new FunctionPoint(1, 19);
            exampleTabFunc.addPoint(examplePoint);
            exampleTabFunc.print();*/
        } catch (FunctionPointIndexOutOfBoundsException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } /*catch (InappropriateFunctionPointException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }
}
