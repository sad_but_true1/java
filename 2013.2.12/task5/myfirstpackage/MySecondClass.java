package myfirstpackage;

public class MySecondClass
{
	private int x=0, y=0;
	public int getValueX()
	{
		return x;
	}
	public int getValueY()
	{
		return y;
	}
	public void setValueX(int a)
	{
		x=a;
	}
	public void setValueY(int a)
	{
		y=a;
	}
	public MySecondClass (int a, int b)
	{
		x=a;
		y=b;
	}
	public int mult()
	{
		return x*y;
	}
	public int com()
	{
		return x+y;
	}
	public int sub()
	{
		return x-y;
	}
	public int div()
	{
		if (y!=0) return x/y;
		else {System.out.println("NO WAY!");return 0;}
	}
}
