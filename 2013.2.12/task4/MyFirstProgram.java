public class MyFirstProgram
{
	public static void main (String[] args)
	{
		MySecondClass o=new MySecondClass(4,2);
		int i,j;
		for (i=1; i<=8; i++)
		{
			for (j=1; j<=8; j++)
			{
				o.setValueX(i);
				o.setValueY(j);
				System.out.print(o.mult());
				System.out.print(" ");
			}
			System.out.println();
		}
	}
}

class MySecondClass
{
	private int x=0, y=0;
	public int getValueX()
	{
		return x;
	}
	public int getValueY()
	{
		return y;
	}
	public void setValueX(int a)
	{
		x=a;
	}
	public void setValueY(int a)
	{
		y=a;
	}
	MySecondClass (int a, int b)
	{
		x=a;
		y=b;
	}
	public int mult()
	{
		return x*y;
	}
	public int com()
	{
		return x+y;
	}
	public int sub()
	{
		return x-y;
	}
	public int div()
	{
		if (y!=0) return x/y;
		else {System.out.println("NO WAY!");return 0;}
	}
}
