/**
Написать класс матриц, поддерживающий операцию транспонирования.
Должны быть описан один класс, который должен:
инкапсулировать в себе массив-матрицу;
иметь конструктор, параметрами которого являются размеры матрицы;
реализовывать получение размеров матрицы;
реализовывать получение и изменение отдельных элементов матрицы;
иметь статический метод транспонирования матриц, возвращающий транспонированную матрицу.

TODO а какой тип?
*/

public class ex2 {
  public static void main(String[] args) {
     MatrixWithTranspose test = new MatrixWithTranspose(10, 3);
     System.out.println("" + test.getRowCount() + " " + test.getColumnCount());
     test.setElement(1, 0, 10298);
     MatrixWithTranspose transposeTest = MatrixWithTranspose.transpose(test);
     for (int i = 0; i < test.getRowCount(); i++) {
      for (int j = 0; j < test.getColumnCount(); j++) {
	System.out.print("" + test.getElement(i, j) + " ");
      }
      System.out.println();
     }
     for (int i = 0; i < transposeTest.getRowCount(); i++) {
      for (int j = 0; j < transposeTest.getColumnCount(); j++) {
	System.out.print("" + transposeTest.getElement(i, j) + " ");
      }
      System.out.println();
     }
  }
}

class MatrixWithTranspose {
  private int[][] matrix;
  
  public MatrixWithTranspose(int rowCount, int columnCount) {
    matrix = new int [rowCount][columnCount];
  }
  
  public int getRowCount() {
    return matrix.length;
  }
  
  public int getColumnCount() {
    return matrix[0].length;
  }
  
  public int getElement(int row, int column) {
    return matrix[row][column];
  }
  
  public void setElement(int row, int column, int newValue) {
    matrix[row][column] = newValue;
  }
  
  public static MatrixWithTranspose transpose (MatrixWithTranspose matrix) {
    MatrixWithTranspose tmp = new MatrixWithTranspose( matrix.getColumnCount(), matrix.getRowCount());
    for (int i = 0; i < matrix.getRowCount(); i++) {
      for (int j = 0; j < matrix.getColumnCount(); j++) {
	tmp.setElement(j, i, matrix.getElement(i, j));
      }
    }
    return tmp;
  }
}