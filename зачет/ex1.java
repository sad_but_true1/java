import java.io.IOException;

/**
Написать программу, вычисляющую среднее, а также минимальное и максимальное значения вещественных чисел, переданных как параметры при запуске программы. Полученные три числа следует вывести на экран (консоль).
Рекомендуется писать программу, состоящую из одного класса, содержащего точку входа программы. Параметры при запуске могут указываться с ошибками.
*/

public class ex1 {
  public static void main (String[] args) {
    double x = 0;
    double y = 0;
    double z = 0;
    try {
      x = Double.parseDouble(args[0]);
      y = Double.parseDouble(args[1]);
      z = Double.parseDouble(args[2]);
      System.out.println("Average = " + average(x, y, z) + "\nMax = " + max(x, y, z) + "\nMin = " + min(x, y, z));
    } catch (NumberFormatException ex) {
	System.err.println("Wrong args");
    }
  }
  
  private static double average(double x1, double x2, double x3) {
    return (x1 + x2 + x3)/3;
  }
  
  private static double max(double x1, double x2, double x3) {
    if (x1 > x2 && x1 > x3) {
      return x1;
    } else if (x2 > x3) {
	    return x2;
	   } else {
	      return x3;
	   }
  }
  private static double min(double x1, double x2, double x3) {
    if (x1 < x2 && x1 < x3) {
      return x1;
    } else if (x2 < x3) {
	    return x2;
	   } else {
	      return x3;
	   }
  }  
}
