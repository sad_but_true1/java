/**
Описать интерфейс функций, разрешающий выполнение трёх операций:
получение левой границы области определения;
получение правой границы области определения;
вычисление значения функции в заданной точке.
Написать базовый абстрактный класс для тригонометрических функций, реализующий методы получения границ области определения.
Написать два наследующих от него класса функций для Sin2(x) и Cos2(x).
Написать класс функции, являющейся суммой двух переданных в конструктор функций.
В методе main() создать объект для функции суммы Sin2(x) и Cos2(x), сохранить ссылку на него в поле интерфейсного типа. Вывести в консоль значения этой функции от 0 до 2π с шагом 0,1.
*/

public class ex3 {
  public static void main (String[] args)
  {
    Function test = new Sum(new Sin(), new Cos());
    for (double delta = 0; !(delta > 2*Math.PI); delta += 0.1) {
      System.out.println(test.getFunctionValue(delta));
    }
  }
}

interface Function {
  public double getLeftDomainBorder();
  public double getRightDomainBorder();
  public double getFunctionValue(double x);
}

abstract class TrigonometricFunction implements Function {
  public double getLeftDomainBorder() {
     return Double.NEGATIVE_INFINITY;
  }
  
  public double getRightDomainBorder() {
     return Double.POSITIVE_INFINITY;
  }
}

class Sin extends TrigonometricFunction {
  public double getFunctionValue(double x) {
    return Math.sin(x) * Math.sin(x);
  }
}

class Cos extends TrigonometricFunction {
  public double getFunctionValue(double x) {
    return Math.cos(x) * Math.cos(x);
  }
}

class Sum implements Function {
  private Function func1, func2;

  public Sum(Function func1, Function func2) {
    this.func1 = func1;
    this.func2 = func2;
  }
  
  public double getLeftDomainBorder() {
    return func1.getLeftDomainBorder() < func2.getLeftDomainBorder() ? func2.getLeftDomainBorder() : func1.getLeftDomainBorder();
  }
  
  public double getRightDomainBorder() {
    return func1.getRightDomainBorder() < func2.getRightDomainBorder() ? func1.getRightDomainBorder() : func2.getRightDomainBorder();
  }
  
  public double getFunctionValue(double x) {
    return func1.getFunctionValue(x) + func2.getFunctionValue(x);
  }
}