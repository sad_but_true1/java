/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Iterator;
import functions.basic.Sin;
import functions.basic.Cos;
import functions.*;
import functions.basic.Exp;
import functions.basic.Log;
import threads.Generator;
import threads.Integrator;
import threads.Semaphore;
import threads.SimpleGenerator;
import threads.SimpleIntegrator;
import threads.Task;
import static java.lang.Math.random;

/**
 *
 * @author true
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        double x = Functions.integrate(new Exp(), 0, 1, 0.0000001);
//        System.out.println(x);
//        System.out.println("Non thread");
//        nonThread();
//        System.out.println("Simple");
//        simpleThreads();
//        System.out.println("Complicated");
//        complicatedThreads();
        TabulatedFunction func = new ArrayTabulatedFunction(0, 10, 11);
        for (FunctionPoint point : func) {
            System.out.println(point);
        }

        System.out.println();

        func = new LinkedListTabulatedFunction(11, 21, 11);
        for (FunctionPoint point : func) {
            System.out.println(point);
        }

        System.out.println();

        Function f = new Cos();
        TabulatedFunction tf;
        tf = TabulatedFunctions.tabulate(f, 0, Math.PI, 11);
        System.out.println(tf.getClass());
        TabulatedFunctions.setTabulatedFunctionFactory(new LinkedListTabulatedFunction.LinkedListTabulatedFunctionFactory());
        tf = TabulatedFunctions.tabulate(f, 0, Math.PI, 11);
        System.out.println(tf.getClass());
        TabulatedFunctions.setTabulatedFunctionFactory(new ArrayTabulatedFunction.ArrayTabulatedFunctionFactory());
        tf = TabulatedFunctions.tabulate(f, 0, Math.PI, 11);
        System.out.println(tf.getClass());

        System.out.println();
        func = TabulatedFunctions.createTabulatedFunction(
                ArrayTabulatedFunction.class, 0, 10, 3);
        System.out.println(func.getClass());
        System.out.println(func);
        func = TabulatedFunctions.createTabulatedFunction(
                ArrayTabulatedFunction.class, 0, 10, new double[]{0, 10});
        System.out.println(func.getClass());
        System.out.println(func);
        func = TabulatedFunctions.createTabulatedFunction(
                LinkedListTabulatedFunction.class,
                new FunctionPoint[]{
                    new FunctionPoint(0, 0),
                    new FunctionPoint(10, 10)
                });
        System.out.println(func.getClass());
        System.out.println(func);
        func = TabulatedFunctions.tabulate(
                LinkedListTabulatedFunction.class, new Sin(), 0, Math.PI, 11);
        System.out.println(func.getClass());
        System.out.println(func);
        
//        func = TabulatedFunctions.tabulate(
//                double.class, new Sin(), 0, Math.PI, 11);
//        System.out.println(func.getClass());
//        System.out.println(func);

    }

    static void nonThread() {
        Task task = new Task();
        task.count = 100;
        for (int index = 0; index < task.count; index++) {
            Function log = new Log(1 + random() * 9);
            task.func = log;
            task.leftBorder = random() * 100;
            task.rightBorder = random() * 100 + 100;
            task.delta = random();
            System.out.println("Source " + task.leftBorder + " " + task.rightBorder + " " + task.delta);
            double x = Functions.integrate(task.func, task.leftBorder, task.rightBorder, task.delta);
            System.out.println("Result " + task.leftBorder + " " + task.rightBorder + " " + task.delta + " " + x);
        }
    }

    static void simpleThreads() {
        Task task = new Task();
        task.func = new Log(1 + random() * 9);
        task.leftBorder = random() * 100;
        task.rightBorder = random() * 100 + 100;
        task.delta = random();
        task.count = 100;
        Runnable generator = new SimpleGenerator(task);
        Thread generatorThread = new Thread(generator, "Generator");
        //generatorThread.setPriority(Thread.MAX_PRIORITY);
        Runnable integrator = new SimpleIntegrator(task);
        Thread integratorThread = new Thread(integrator, "Integrator");
        //integratorThread.setPriority(Thread.MAX_PRIORITY);
        //System.err.println("" + generatorThread.getPriority() + " " + integratorThread.getPriority());
        generatorThread.start();
        integratorThread.start();
    }

    static void complicatedThreads() {
        Task task = new Task();
        task.count = 100;
        Semaphore semaphore = new Semaphore();
        Thread generatorThread = new Generator(task, semaphore, "Generator");
        //generatorThread.setPriority(Thread.MAX_PRIORITY);
        Thread integratorThread = new Integrator(task, semaphore, "Integrator");
        //integratorThread.setPriority(Thread.MAX_PRIORITY);
        //System.err.println("" + generatorThread.getPriority() + " " + integratorThread.getPriority());
        generatorThread.start();
        integratorThread.start();
        try {
            Thread.sleep(50);
        } catch (InterruptedException ex) {
            System.err.println(ex);
        }
        generatorThread.interrupt();
        integratorThread.interrupt();
    }
}
