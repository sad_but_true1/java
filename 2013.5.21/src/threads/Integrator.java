/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import functions.Functions;

/**
 *
 * @author true
 */
public class Integrator extends Thread {

    private final Task task;
    private Semaphore semaphore;

    public Integrator(Task task, Semaphore semaphore, String name) {
        super(name);
        this.task = task;
        this.semaphore = semaphore;
    }

    public void run() {
        for (int index = 0; index < task.count && !isInterrupted(); index++) {
            try {
                semaphore.beginRead();
            } catch (InterruptedException ex) {
                interrupt();
            }
            double x = Functions.integrate(task.func, task.leftBorder, task.rightBorder, task.delta);
            System.out.println("Result " + task.leftBorder + " " + task.rightBorder + " " + task.delta + " " + x + " " + index);
            semaphore.endRead();
        }
    }
}
