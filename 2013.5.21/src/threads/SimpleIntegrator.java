/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import functions.Functions;

/**
 *
 * @author true
 */
public class SimpleIntegrator implements Runnable {

    private final Task task;

    public SimpleIntegrator(Task task) {
        this.task = task;
    }

    @Override
    public void run() {
        for (int index = 0; index < task.count; index++) {
            synchronized (task) {
                double x = Functions.integrate(task.func, task.leftBorder, task.rightBorder, task.delta);
                System.out.println("Result " + task.leftBorder + " " + task.rightBorder + " " + task.delta + " " + x);
            }
        }
    }
}
