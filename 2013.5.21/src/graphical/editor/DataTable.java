/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphical.editor;

import functions.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

/**
 *
 * @author true
 */
public class DataTable implements TabulatedFunction {

    private boolean modified, fileNameAssigned;
    private TabulatedFunction tabulatedFunction;
    private String fileName;

    public DataTable(TabulatedFunction func) {
        tabulatedFunction = func;
        modified = true;
        fileNameAssigned = false;
    }

    public DataTable(TabulatedFunction func, String fileName) {
        tabulatedFunction = func;
        modified = true;
        this.fileName = fileName;
        fileNameAssigned = true;
    }

    public void newFunction(double leftX, double rightX, int pointsCount) {
        tabulatedFunction = new ArrayTabulatedFunction(leftX, rightX, pointsCount);
    }

    public void saveFunction() throws IOException {
        saveFunctionAs(fileName);
    }

    public void saveFunctionAs(String fileName) throws IOException {
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName)));
        TabulatedFunctions.writeTabulatedFunction(tabulatedFunction, out);
        out.close();
        modified = false;
        fileNameAssigned = true;
    }

    public void loadFunction(String fileName) throws FileNotFoundException, IOException {
        BufferedReader in = new BufferedReader(new FileReader(fileName));
        tabulatedFunction = TabulatedFunctions.readTabulatedFunction(in);
        in.close();
        modified = false;
    }

    public void tabulateFunction(Function function, double leftX, double rightX, int pointsCount) {
        tabulatedFunction = TabulatedFunctions.tabulate(function, leftX, rightX, pointsCount);
    }

    public boolean isModified() {
        return modified;
    }

    public boolean isFileNameAssigned() {
        return fileNameAssigned;
    }

    @Override
    public int getPointsCount() {
        return tabulatedFunction.getPointsCount();
    }

    @Override
    public FunctionPoint getPoint(int index) {
        return tabulatedFunction.getPoint(index);
    }

    @Override
    public double getPointX(int index) {
        return tabulatedFunction.getPointX(index);
    }

    @Override
    public void setPointX(int index, double x) throws InappropriateFunctionPointException {
        tabulatedFunction.setPointX(index, x);
        modified = true;
    }

    @Override
    public double getPointY(int index) {
        return tabulatedFunction.getPointY(index);
    }

    @Override
    public void setPointY(int index, double y) {
        tabulatedFunction.setPointY(index, y);
    }

    @Override
    public void setPoint(int index, FunctionPoint point) throws InappropriateFunctionPointException {
        tabulatedFunction.setPoint(index, point);
        modified = true;
    }

    @Override
    public void deletePoint(int index) {
        tabulatedFunction.deletePoint(index);
        modified = true;
    }

    @Override
    public void addPoint(FunctionPoint point) throws InappropriateFunctionPointException {
        tabulatedFunction.addPoint(point);
        modified = true;
    }

    @Override
    public double getLeftDomainBorder() {
        return tabulatedFunction.getLeftDomainBorder();
    }

    @Override
    public double getRightDomainBorder() {
        return tabulatedFunction.getRightDomainBorder();
    }

    @Override
    public double getFunctionValue(double x) {
        return tabulatedFunction.getFunctionValue(x);
    }

    @Override
    public Object clone() {
        Object result = null;
        try {
            result = super.clone();
            ((DataTable) result).tabulatedFunction = (TabulatedFunction) tabulatedFunction.clone();
        } catch (CloneNotSupportedException e) {
            System.err.println(e);
        }
        return result;
    }

    public String toString() {
        return tabulatedFunction.toString();
    }

    public boolean equals(Object o) {
        if (!(o instanceof TabulatedFunction)) {
            return false;
        }
        if (o == tabulatedFunction) {
            return true;
        }

        if (o instanceof DataTable) {
            return (((DataTable) o).modified == modified && ((DataTable) o).fileNameAssigned == fileNameAssigned
                    && ((DataTable) o).fileName.equals(fileName) && tabulatedFunction.equals(((DataTable) o).tabulatedFunction));
        } else if (o instanceof TabulatedFunction) {
            return ((TabulatedFunction) o).equals(tabulatedFunction);
        }
        return true;
    }

    public int hashCode() {
        return tabulatedFunction.hashCode();
    }

    @Override
    public Iterator<FunctionPoint> iterator() {
        return tabulatedFunction.iterator();
    }
}
