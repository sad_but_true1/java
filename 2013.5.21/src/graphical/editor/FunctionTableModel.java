/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphical.editor;

import functions.FunctionPointIndexOutOfBoundsException;
import functions.InappropriateFunctionPointException;
import functions.TabulatedFunction;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author true
 */
public class FunctionTableModel extends DefaultTableModel {

    private TabulatedFunction tabulatedFunction;
    private Component parent;

    public FunctionTableModel(TabulatedFunction func, Component parent) {
        super();
        tabulatedFunction = func;
        this.parent = parent;
    }

    public int getRowCount() {
        if (tabulatedFunction == null) {
            return 0;
        } else {
            return tabulatedFunction.getPointsCount();
        }
    }

    public int getColumnCount() {
        return 2;
    }

    public String getColumnName(int index) {
        return (index == 0) ? "X" : "Y";
    }

    public Class getColumnClass(int index) {
        return Double.class;
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return tabulatedFunction.getPointX(rowIndex);
        } else {
            return tabulatedFunction.getPointY(rowIndex);
        }
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        if (!(value instanceof Double)) {
            JOptionPane.showMessageDialog(parent, "Wrong object!");
        }
        try {
            if (columnIndex == 0) {
                tabulatedFunction.setPointX(rowIndex, (Double) value);
            } else {
                tabulatedFunction.setPointY(rowIndex, (Double) value);
            }
        } catch (FunctionPointIndexOutOfBoundsException | InappropriateFunctionPointException e) {
            JOptionPane.showMessageDialog(parent, e);
        }
    }
}
