/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions.meta;

import functions.Function;
/**
 *
 * @author true
 */
public class Composition implements Function{
    private Function func1, func2;
    
    public Composition(Function func1, Function func2){
        this.func1 = func1;
        this.func2 = func2;
    }
    
    public double getLeftDomainBorder(){
        return func2.getLeftDomainBorder();
    }

    public double getRightDomainBorder(){
        return func2.getRightDomainBorder();
    }

    public double getFunctionValue(double x){
        return func1.getFunctionValue(func2.getFunctionValue(x));
    }
}
