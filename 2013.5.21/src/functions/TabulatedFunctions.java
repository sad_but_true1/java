/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;

/**
 *
 * @author true
 */
public class TabulatedFunctions {

    private TabulatedFunctions() {
    }
    private static TabulatedFunctionFactory factory = new ArrayTabulatedFunction.ArrayTabulatedFunctionFactory();

    public static void setTabulatedFunctionFactory(TabulatedFunctionFactory factory) {
        TabulatedFunctions.factory = factory;
    }

    public static TabulatedFunction createTabulatedFunction(double leftX, double rightX, int pointsCount) {
        return factory.createTabulatedFunction(leftX, rightX, pointsCount);
    }

    public static TabulatedFunction createTabulatedFunction(double leftX, double rightX, double[] values) {
        return factory.createTabulatedFunction(leftX, rightX, values);
    }

    public static TabulatedFunction createTabulatedFunction(FunctionPoint[] points) {
        return factory.createTabulatedFunction(points);
    }

    public static TabulatedFunction createTabulatedFunction(Class <? extends TabulatedFunction> inClass, double leftX, double rightX, int pointsCount) {
//        Class[] inClassInterfaces = inClass.getInterfaces();
//        boolean isTF = false;
//        for (int index = 0; index < inClassInterfaces.length; index++) {
//            if (inClass.getInterfaces()[index] == TabulatedFunction.class) {
//                isTF = true;
//                break;
//            }
//        }
//        if (!isTF) {
//            throw new IllegalArgumentException("It's not TF!");
//        }
        TabulatedFunction tmp = null;
        try {
            Class[] constructorArgs = {double.class, double.class, int.class};
            Object[] instanceArgs = {leftX, rightX, pointsCount};
            tmp = inClass.getConstructor(constructorArgs).newInstance(instanceArgs);
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new IllegalArgumentException(ex);
        }
        return tmp;
    }

    public static TabulatedFunction createTabulatedFunction(Class <? extends TabulatedFunction>  inClass, double leftX, double rightX, double[] values) {
//        Class[] inClassInterfaces = inClass.getInterfaces();
//        boolean isTF = false;
//        for (int index = 0; index < inClassInterfaces.length; index++) {
//            if (inClass.getInterfaces()[index] == TabulatedFunction.class) {
//                isTF = true;
//                break;
//            }
//        }
//        if (!isTF) {
//            throw new IllegalArgumentException("It's not TF!");
//        }
        TabulatedFunction tmp = null;
        try {
            Class[] constructorArgs = {double.class, double.class, double[].class};
            Object[] instanceArgs = {leftX, rightX, values};
            tmp = (TabulatedFunction) inClass.getConstructor(constructorArgs).newInstance(instanceArgs);
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new IllegalArgumentException(ex);
        }
        return tmp;
    }

    public static TabulatedFunction createTabulatedFunction(Class <? extends TabulatedFunction> inClass, FunctionPoint[] points) {
//        Class[] inClassInterfaces = inClass.getInterfaces();
//        boolean isTF = false;
//        for (int index = 0; index < inClassInterfaces.length; index++) {
//            if (inClass.getInterfaces()[index] == TabulatedFunction.class) {
//                isTF = true;
//                break;
//            }
//        }
//        if (!isTF) {
//            throw new IllegalArgumentException("It's not TF!");
//        }
        TabulatedFunction tmp = null;
        try {
            Class[] constructorArgs = {FunctionPoint[].class};
            Object[] instanceArgs = {points};
            tmp = (TabulatedFunction) inClass.getConstructor(constructorArgs).newInstance(instanceArgs);
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new IllegalArgumentException(ex);
        }
        return tmp;
    }

    public static TabulatedFunction tabulate(Function function, double leftX, double rightX, int pointsCount) {
        if (leftX < function.getLeftDomainBorder() || rightX > function.getRightDomainBorder()) {
            throw new IllegalArgumentException("Wrong X domain.");
        }
        FunctionPoint[] functionPoints = new FunctionPoint[pointsCount];
        double delta = (rightX - leftX) / (pointsCount - 1);
        for (int i = 0; i < pointsCount; i++) {
            functionPoints[i] = new FunctionPoint(leftX + i * delta, function.getFunctionValue(leftX + i * delta));
        }
        return createTabulatedFunction(functionPoints);
    }

    public static TabulatedFunction tabulate(Class <? extends TabulatedFunction> inClass, Function function, double leftX, double rightX, int pointsCount) {
        if (leftX < function.getLeftDomainBorder() || rightX > function.getRightDomainBorder()) {
            throw new IllegalArgumentException("Wrong X domain.");
        }
        FunctionPoint[] functionPoints = new FunctionPoint[pointsCount];
        double delta = (rightX - leftX) / (pointsCount - 1);
        for (int i = 0; i < pointsCount; i++) {
            functionPoints[i] = new FunctionPoint(leftX + i * delta, function.getFunctionValue(leftX + i * delta));
        }
        return createTabulatedFunction(inClass, functionPoints);
    }

    public static void outputTabulatedFunction(TabulatedFunction function, OutputStream out) throws IOException {
        DataOutputStream outStream = new DataOutputStream(out);
        int pointsCount = function.getPointsCount();
        outStream.writeInt(pointsCount);
        for (int i = 0; i < pointsCount; i++) {
            outStream.writeDouble(function.getPointX(i));
            outStream.writeDouble(function.getPointY(i));
        }
    }

    public static TabulatedFunction inputTabulatedFunction(InputStream in) throws IOException {
        DataInputStream inStream = new DataInputStream(in);
        int pointsCount = inStream.readInt();
        FunctionPoint[] points = new FunctionPoint[pointsCount];
        for (int i = 0; i < pointsCount; i++) {
            points[i] = new FunctionPoint();
            points[i].x = inStream.readDouble();
            points[i].y = inStream.readDouble();
        }
        return createTabulatedFunction(points);
    }

    public static void writeTabulatedFunction(TabulatedFunction function, Writer out) {
        PrintWriter outPrint = new PrintWriter(out);
        int pointsCount = function.getPointsCount();
        outPrint.println(pointsCount);
        for (int i = 0; i < pointsCount; i++) {
            outPrint.print(function.getPointX(i));
            outPrint.print(' ');
            outPrint.println(function.getPointY(i));
        }
    }

    public static TabulatedFunction readTabulatedFunction(Reader in) throws IOException {
        StreamTokenizer inTokenizer = new StreamTokenizer(in);
        inTokenizer.nextToken();
        int pointsCount = (int) inTokenizer.nval;
        FunctionPoint[] points = new FunctionPoint[pointsCount];
        for (int i = 0; i < pointsCount; i++) {
            points[i] = new FunctionPoint();
            inTokenizer.nextToken();
            points[i].x = inTokenizer.nval;
            inTokenizer.nextToken();
            points[i].y = inTokenizer.nval;
        }
        return createTabulatedFunction(points);
    }
}
