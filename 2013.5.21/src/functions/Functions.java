/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

import functions.meta.*;

/**
 *
 * @author true
 */
public class Functions {

    private Functions() {
    }

    public static Function shift(Function f, double shiftX, double shiftY) {
        return new Shift(f, shiftX, shiftY);
    }

    public static Function scale(Function f, double scaleX, double scaleY) {
        return new Scale(f, scaleX, scaleY);
    }

    public static Function power(Function f, double power) {
        return new Power(f, power);
    }

    public static Function sum(Function f1, Function f2) {
        return new Sum(f1, f2);
    }

    public static Function mult(Function f1, Function f2) {
        return new Mult(f1, f2);
    }

    public static Function composition(Function f1, Function f2) {
        return new Composition(f1, f2);
    }

    public static double integrate(Function func, double leftBorder, double rightBorder, double delta) {
        if (leftBorder < func.getLeftDomainBorder() || rightBorder > func.getRightDomainBorder()) {
            throw new IllegalArgumentException("Wrong integration borders");
        }
        double value = 0;
        double x;
        for (x = leftBorder; x < rightBorder; x += delta) {
            value += (func.getFunctionValue(x - delta) + func.getFunctionValue(x)) * delta / 2;
        }
        value += (func.getFunctionValue(x) + func.getFunctionValue(rightBorder)) * (rightBorder - x) / 2;
        return value;
    }
}
