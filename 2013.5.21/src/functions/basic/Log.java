/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions.basic;

import functions.Function;

/**
 *
 * @author true
 */
public class Log implements Function {

    private double base;

    public Log(double base) {
        this.base = base;
    }

    public double getLeftDomainBorder() {
        return 0;
    }

    public double getRightDomainBorder() {
        return Double.POSITIVE_INFINITY;
    }

    public double getFunctionValue(double x) {
        return Math.log(x)/Math.log(base);
    }
}
