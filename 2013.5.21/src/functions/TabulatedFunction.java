/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

import java.io.Serializable;

/**
 *
 * @author true
 */
public interface TabulatedFunction extends Function, Serializable, Cloneable, Iterable<FunctionPoint> {

    int getPointsCount();

    FunctionPoint getPoint(int index);

    double getPointX(int index);

    void setPointX(int index, double x) throws InappropriateFunctionPointException;

    double getPointY(int index);

    void setPointY(int index, double y);

    void setPoint(int index, FunctionPoint point) throws InappropriateFunctionPointException;

    void deletePoint(int index);

    void addPoint(FunctionPoint point) throws InappropriateFunctionPointException;

    Object clone();
}
