import functions.*;

public class Main {

    public static void main(String[] args) {
        double []x = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        TabulatedFunction exampleTabFunc = new TabulatedFunction(0, 10, x);
        for (double y = exampleTabFunc.getLeftDomainBorder() - 1; y < exampleTabFunc.getRightDomainBorder()+2; y+=0.5){
            System.out.println("("+y+','+exampleTabFunc.getFunctionValue(y)+")");
        }
        exampleTabFunc.setPointX(1, -1);
        exampleTabFunc.setPointX(1, 0.9);
        exampleTabFunc.print();
        System.out.println();
        exampleTabFunc.setPointY(1, 3);
        exampleTabFunc.print();
        System.out.println();
        FunctionPoint examplePoint = new FunctionPoint(11,2);
        exampleTabFunc.setPoint(0,examplePoint);
        exampleTabFunc.print();
        System.out.println();
        examplePoint = new FunctionPoint(0,22);
        exampleTabFunc.setPoint(0,examplePoint);
        exampleTabFunc.print();
        exampleTabFunc.deletePoint(4);
        System.out.println();
        exampleTabFunc.print();
        System.out.println();
        examplePoint = new FunctionPoint(1, 19);
        exampleTabFunc.addPoint(examplePoint);
        exampleTabFunc.print();
    }
}
