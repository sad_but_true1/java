package functions;

/**
 * Created with IntelliJ IDEA.
 * User: true
 * Date: 04.03.13
 * Time: 10:17
 * To change this template use File | Settings | File Templates.
 */
public class FunctionPoint {
    private double x,y;
    public double getX(){
        return x;
    }
    public void setX(double x){
        this.x = x;
    }
    public double getY(){
        return y;
    }
    public void setY(double y){
        this.y = y;
    }
    public FunctionPoint(double x, double y){
        this.x = x;
        this.y = y;
    }
    public FunctionPoint(FunctionPoint point){
        x = point.getX();
        y = point.getY();
    }
    public FunctionPoint(){
        x = 0;
        y = 0;
    }
}
