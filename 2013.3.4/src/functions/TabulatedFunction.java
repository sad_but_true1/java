package functions;

/**
 * Created with IntelliJ IDEA.
 * User: true
 * Date: 04.03.13
 * Time: 10:36
 * To change this template use File | Settings | File Templates.
 */
public class TabulatedFunction {
    private FunctionPoint arrayPoints[];
    private int pointsCount = 0;
    public void print(){
        for (int i = 0; i < pointsCount; i++)
            System.out.println("("+this.getPointX(i)+','+this.getPointY(i)+")");
    }
    public TabulatedFunction(double leftX, double rightX, int pointsCount){
        this.pointsCount = pointsCount;
        arrayPoints = new FunctionPoint[2 * pointsCount];
        double delta = (rightX - leftX)/(pointsCount - 1);
        for (int i = 0; i < arrayPoints.length; i++){
            arrayPoints[i] = new FunctionPoint(leftX + i*delta, 0);
        }
    }
    public TabulatedFunction(double leftX, double rightX, double[] values){
        this.pointsCount = values.length;
        arrayPoints = new FunctionPoint[2 * pointsCount];
        double delta = (rightX - leftX)/(pointsCount - 1);
        for (int i = 0; i < values.length; i++){
            arrayPoints[i] = new FunctionPoint(leftX + i*delta, values[i]);
        }
    }
    public double getLeftDomainBorder(){
        return arrayPoints[0].getX();
    }
    public double getRightDomainBorder(){
        return arrayPoints[pointsCount - 1].getX();
    }
    public double getFunctionValue(double X){
        if (X <= this.getRightDomainBorder() && X >= this.getLeftDomainBorder()){
            for (int i = 0; i < arrayPoints.length - 1; i++)
                if (X >= arrayPoints[i].getX()) {
                    double x1 = arrayPoints[i].getX(), x2 = arrayPoints[i + 1].getX(), y1 = arrayPoints[i].getY(), y2 = arrayPoints[i + 1].getY();
                    return (X - x1) * (y2 - y1) / (x2 - x1) + y1;
                }
        }
        return Double.NaN;
    }
    public int getPointsCount(){
        return pointsCount;
    }
    public FunctionPoint getPoint(int index){
        //FunctionPoint pointCopy = new FunctionPoint(arrayPoints[index].getX(), arrayPoints[index].getY());
        return new FunctionPoint(arrayPoints[index]);
    }
    public double getPointX(int index){
        return arrayPoints[index].getX();
    }
    public void setPointX(int index, double X){
        if (index == 0){
            if (X < arrayPoints[index+1].getX()) arrayPoints[index].setX(X);
        }
        else
            if (index == pointsCount - 1){
                if (X > arrayPoints[index-1].getX()) arrayPoints[index].setX(X);
            }
            else
                if (arrayPoints[index-1].getX() < X && X < arrayPoints[index+1].getX()) arrayPoints[index].setX(X);
                else
                    System.out.println("Wrong x domain.\n");
    }
    public double getPointY(int index){
        return arrayPoints[index].getY();
    }
    public void setPointY(int index, double y){
        arrayPoints[index].setY(y);
    }
    public void setPoint(int index, FunctionPoint point){
        double X = point.getX();
        FunctionPoint pointCopy = new FunctionPoint(point.getX(), point.getY());
        if (index == 0){
            if (X <= arrayPoints[index+1].getX())
                arrayPoints[index] = pointCopy;
            else
                System.out.println("Wrong x domain.");
        }
        else
        if (index == pointsCount - 1){
            if (X >= arrayPoints[index-1].getX())
                arrayPoints[index] = pointCopy;
            else
                System.out.println("Wrong x domain.");
        }
        else
            if (arrayPoints[index-1].getX() <= X && X <= arrayPoints[index+1].getX())
                arrayPoints[index] = pointCopy;
            else
                System.out.println("Wrong x domain.");
    }
    public void deletePoint(int index){
        if (index < pointsCount - 1)
            System.arraycopy(arrayPoints, index + 1, arrayPoints, index, pointsCount - 1 - index);
        pointsCount--;
    }
    public void addPoint(FunctionPoint point){
        FunctionPoint pointCopy = new FunctionPoint(point.getX(), point.getY());
        if (pointsCount >= arrayPoints.length){
            FunctionPoint tmp[];
            tmp = new FunctionPoint[arrayPoints.length*2];
            System.arraycopy(arrayPoints, 0, tmp, 0, arrayPoints.length);
            arrayPoints = tmp;
        }
        int i=0;
        while (arrayPoints[i].getX() < pointCopy.getX() && i < pointsCount)
            i++;
        System.arraycopy(arrayPoints, i, arrayPoints, i+1, pointsCount - i);
        pointsCount++;
        setPoint(i, pointCopy);
    }
}
