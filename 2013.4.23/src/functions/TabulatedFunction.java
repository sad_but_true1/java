/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

import java.io.Serializable;

/**
 *
 * @author true
 */
public interface TabulatedFunction extends Function, Serializable, Cloneable {

    public int getPointsCount();

    public FunctionPoint getPoint(int index);

    public double getPointX(int index);

    public void setPointX(int index, double x) throws InappropriateFunctionPointException;

    public double getPointY(int index);

    public void setPointY(int index, double y);

    public void setPoint(int index, FunctionPoint point) throws InappropriateFunctionPointException;

    public void deletePoint(int index);

    public void addPoint(FunctionPoint point) throws InappropriateFunctionPointException;

    public Object clone();
}
