/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions.meta;

import functions.Function;

/**
 *
 * @author true
 */
public class Shift implements Function {

    private double shiftX, shiftY;
    private Function func;

    public Shift(Function func, double shiftX, double shiftY) {
        this.func = func;
        this.shiftX = shiftX;
        this.shiftY = shiftY;
    }

    public double getLeftDomainBorder() {
        return func.getLeftDomainBorder() + shiftX;
    }

    public double getRightDomainBorder() {
        return func.getRightDomainBorder() + shiftX;
    }

    public double getFunctionValue(double x) {
        return func.getFunctionValue(x - shiftX) + shiftY;
    }
}
