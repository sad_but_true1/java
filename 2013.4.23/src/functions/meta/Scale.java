/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions.meta;

import functions.Function;

/**
 *
 * @author true
 */
public class Scale implements Function {

    private double scaleX, scaleY;
    private Function func;

    public Scale(Function func, double scaleX, double scaleY) {
        this.func = func;
        this.scaleX = scaleX;
        this.scaleY = scaleY;
    }

    public double getLeftDomainBorder() {
        return func.getLeftDomainBorder() * scaleX;
    }

    public double getRightDomainBorder() {
        return func.getRightDomainBorder() * scaleX;
    }

    public double getFunctionValue(double x) {
        return func.getFunctionValue(x/scaleX) * scaleY;
    }
}
