/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.io.Writer;

/**
 *
 * @author true
 */
public class TabulatedFunctions {

    private TabulatedFunctions() {
    }

    public static TabulatedFunction tabulate(Function function, double leftX, double rightX, int pointsCount) {
        if (leftX < function.getLeftDomainBorder() || rightX > function.getRightDomainBorder()) {
            throw new IllegalArgumentException("Wrong X domain.");
        }
        FunctionPoint[] functionPoints = new FunctionPoint[pointsCount];
        double delta = (rightX - leftX) / (pointsCount - 1);
        for (int i = 0; i < pointsCount; i++) {
            functionPoints[i] = new FunctionPoint(leftX + i * delta, function.getFunctionValue(leftX + i * delta));
        }
        return new ArrayTabulatedFunction(functionPoints);
    }

    public static void outputTabulatedFunction(TabulatedFunction function, OutputStream out) throws IOException {
        DataOutputStream outStream = new DataOutputStream(out);
        int pointsCount = function.getPointsCount();
        outStream.writeInt(pointsCount);
        for (int i = 0; i < pointsCount; i++) {
            outStream.writeDouble(function.getPointX(i));
            outStream.writeDouble(function.getPointY(i));
        }
    }

    public static TabulatedFunction inputTabulatedFunction(InputStream in) throws IOException {
        DataInputStream inStream = new DataInputStream(in);
        int pointsCount = inStream.readInt();
        FunctionPoint[] points = new FunctionPoint[pointsCount];
        for (int i = 0; i < pointsCount; i++) {
            points[i] = new FunctionPoint();
            points[i].x = inStream.readDouble();
            points[i].y = inStream.readDouble();
        }
        return new ArrayTabulatedFunction(points);
    }

    public static void writeTabulatedFunction(TabulatedFunction function, Writer out) {
        PrintWriter outPrint = new PrintWriter(out);
        int pointsCount = function.getPointsCount();
        outPrint.println(pointsCount);
        for (int i = 0; i < pointsCount; i++) {
            outPrint.print(function.getPointX(i));
            outPrint.print(' ');
            outPrint.println(function.getPointY(i));
        }
    }

    public static TabulatedFunction readTabulatedFunction(Reader in) throws IOException {
        StreamTokenizer inTokenizer = new StreamTokenizer(in);
        inTokenizer.nextToken();
        int pointsCount = (int) inTokenizer.nval;
        FunctionPoint[] points = new FunctionPoint[pointsCount];
        for (int i = 0; i < pointsCount; i++) {
            points[i] = new FunctionPoint();
            inTokenizer.nextToken();
            points[i].x = inTokenizer.nval;
            inTokenizer.nextToken();
            points[i].y = inTokenizer.nval;
        }
        return new ArrayTabulatedFunction(points);
    }
}
