/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

/**
 *
 * @author true
 */
public interface Function {

    public double getLeftDomainBorder();

    public double getRightDomainBorder();

    public double getFunctionValue(double x);
}
